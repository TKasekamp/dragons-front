# dragons-front

[![pipeline status](https://gitlab.com/TKasekamp/dragons-front/badges/master/pipeline.svg)](https://gitlab.com/TKasekamp/dragons-front/commits/master)


Frontend for https://www.dragonsofmugloar.com/

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Style
Using React-md with instructions from [here](https://github.com/mlaursen/react-md/tree/master/examples/with-create-react-app)
to compile scss.
