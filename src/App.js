import React, {Component} from 'react';
import Toolbar from 'react-md/lib/Toolbars/Toolbar';
import GameContainer from './containers/GameContainer';
import Toast from './components/Toast';

class App extends Component {
  render() {
    return (
      <div>
        <Toolbar colored fixed title="Dragons of Mugloar" />
        <div className="md-grid md-toolbar-relative">
          <GameContainer />
          <Toast />
        </div>
      </div>
    );
  }
}

export default App;
