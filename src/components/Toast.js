import React from 'react';
import {Snackbar} from 'react-md';
import PropTypes from 'prop-types';
import connect from 'react-redux/es/connect/connect';
import {TOAST_DISMISSED} from '../actions/GameActions';

const Toast = ({toasts, onDismiss}) => (<Snackbar
  id="example-snackbar"
  toasts={toasts}
  onDismiss={onDismiss}
  autohide={true}
/>);

Toast.propTypes = {
  toasts: PropTypes.array.isRequired,
  onDismiss: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
  toasts: state.toasts
});

const mapDispatchToProps = (dispatch) => ({
  onDismiss: () => dispatch({type: TOAST_DISMISSED})
});

export default connect(mapStateToProps, mapDispatchToProps)(Toast);
