import React from 'react';
import {Card, CardText, CardTitle} from 'react-md/lib/Cards/index';
import Button from 'react-md/lib/Buttons/Button';
import PropTypes from 'prop-types';

export const ReputationCard = props => {
  const {people, state, underworld} = props.reputation;
  return <Card className="md-cell md-cell--4 md-cell--6-tablet">
    <CardTitle title="Reputation" />
    <CardText className="md-grid">
      <div className="md-cell md-cell--12">
        <span className="md-cell"><b>People:</b> {people} </span>
        <span className="md-cell"><b>State:</b> {state} </span>
        <span className="md-cell"><b>Underworld:</b> {underworld} </span>
      </div>
      <Button raised primary swapTheming onClick={() => props.onGetReputation(props.gameId)}
              className="buttons__group">Request</Button>
    </CardText>
  </Card>;
};

ReputationCard.propTypes = {
  gameId: PropTypes.string.isRequired,
  reputation: PropTypes.shape({
    people: PropTypes.number,
    state: PropTypes.number,
    underworld: PropTypes.number
  }).isRequired,
  onGetReputation: PropTypes.func.isRequired
};
