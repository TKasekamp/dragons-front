import React from 'react';
import {DataTable, TableBody, TableColumn, TableHeader, TableRow} from 'react-md/lib/DataTables/index';
import PropTypes from 'prop-types';
import {Card, CardText, CardTitle} from 'react-md/lib/Cards/index';
import Button from 'react-md/lib/Buttons/Button';

const tableBody = ({items, gameId, onPurchaseItem}) => {
  return items.map((item) => {
    const bought = item.shoppingSuccess ? 'Yes' : 'No';

    return (
      <TableRow key={item.id} selectable={false}>
        <TableColumn>
          <Button raised primary swapTheming onClick={() => onPurchaseItem(gameId, item.id)}
                  className="buttons__group">Buy</Button>
        </TableColumn>
        <TableColumn>{item.name}</TableColumn>
        <TableColumn numeric>{item.cost}</TableColumn>
        <TableColumn>{bought}</TableColumn>
      </TableRow>
    );
  });
};

const ItemsTableCard = (props) => {
  const headers = ['', 'Name', 'Cost', 'Shopping success'];

  return (
    <Card className="md-cell md-cell--12 md-cell--12-tablet">
      <CardTitle title="Shop items" />
      <CardText>
        <Button raised primary swapTheming onClick={() => props.onGetItems(props.gameId)}
                className="buttons__group">Get shop items</Button>
        <DataTable baseId="simple-pagination" plain>
          <TableHeader>
            <TableRow selectable={false}>
              {headers.map((header) => <TableColumn key={header}> {header}</TableColumn>)}
            </TableRow>
          </TableHeader>
          <TableBody>
            {tableBody(props)}
          </TableBody>
        </DataTable>
      </CardText>
    </Card>);
};

const itemPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  cost: PropTypes.number.isRequired,
  shoppingSuccess: PropTypes.bool
}).isRequired;

ItemsTableCard.propTypes = {
  items: PropTypes.arrayOf(itemPropType).isRequired,
  gameId: PropTypes.string.isRequired,
  onGetItems: PropTypes.func.isRequired,
  onPurchaseItem: PropTypes.func.isRequired
};

export default ItemsTableCard;
