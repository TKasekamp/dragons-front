import React from 'react';
import {Card, CardText} from 'react-md/lib/Cards/index';
import PropTypes from 'prop-types';
import Button from 'react-md/lib/Buttons/Button';
import {ReputationCard} from './ReputationCard';
import {GameInfoCard} from './GameInfoCard';
import MessagesTableCard from './MessagesTableCard';
import CardTitle from 'react-md/lib/Cards/CardTitle';
import ItemsTableCard from './ItemsTableCard';

export const Game = props => {
  const StartGame = ({onCreateGame}) => (
    <Card className="md-cell md-cell--4 md-cell--6-tablet">
      <CardTitle title="Welcome to this exciting game!" subtitle="Frontend created by Tõnis Kasekamp"/>
      <CardText>
        <Button raised primary swapTheming onClick={onCreateGame}
                className="buttons__group">Start new game</Button>
      </CardText>
    </Card>
  );

  // No game, only showing start button
  // ideally would show empty cards,
  // but don't have time
  if (!props.game.gameId) {
    return (<StartGame {...props} />);
  }

  return [
    <StartGame key="s" {...props} />,
    <ReputationCard key="r" onGetReputation={props.onGetReputation} {...props.game} />,
    <GameInfoCard key="i" {...props.game} />,
    <MessagesTableCard key="m" onGetMessages={props.onGetMessages}
                       onSolveMessage={props.onSolveMessage} {...props.game} />,
    <ItemsTableCard key="items" onGetItems={props.onGetItems}
                    onPurchaseItem={props.onPurchaseItem} {...props.game} />

  ];
};

Game.propTypes = {
  game: PropTypes.shape({
    gameId: PropTypes.string
  }),
  onCreateGame: PropTypes.func.isRequired,
  onGetReputation: PropTypes.func.isRequired,
  onGetMessages: PropTypes.func.isRequired,
  onSolveMessage: PropTypes.func.isRequired,
  onGetItems: PropTypes.func.isRequired,
  onPurchaseItem: PropTypes.func.isRequired,
};
