import React from 'react';
import {DataTable, TableBody, TableColumn, TableHeader, TableRow} from 'react-md/lib/DataTables/index';
import PropTypes from 'prop-types';
import {Card, CardText, CardTitle} from 'react-md/lib/Cards/index';
import Button from 'react-md/lib/Buttons/Button';

const tableBody = ({messages, gameId, onSolveMessage}) => {
  return messages.map((message) => {
    let firstColumn;
    if (message.success === undefined) {
      firstColumn = <Button raised primary swapTheming onClick={() => onSolveMessage(gameId, message.adId)}
                            className="buttons__group">Solve</Button>;
    } else {
      firstColumn = message.success ? 'Yes' : 'No';
    }

    return (
      <TableRow key={message.adId} selectable={false}>
        <TableColumn>{firstColumn}</TableColumn>
        <TableColumn>{message.message}</TableColumn>
        <TableColumn numeric>{message.reward}</TableColumn>
        <TableColumn numeric>{message.expiresIn}</TableColumn>
        <TableColumn>{message.probability}</TableColumn>
      </TableRow>
    );
  });
};

const MessagesTableCard = (props) => {
  const headers = ['', 'Message', 'Reward', 'Expires in', 'Probability'];

  return (
    <Card className="md-cell md-cell--12 md-cell--12-tablet">
      <CardTitle title="Messages" />
      <CardText>
        <Button raised primary swapTheming onClick={() => props.onGetMessages(props.gameId)}
                className="buttons__group">Get messages</Button>
        <DataTable baseId="simple-pagination" plain>
          <TableHeader>
            <TableRow selectable={false}>
              {headers.map((header) => <TableColumn key={header}> {header}</TableColumn>)}
            </TableRow>
          </TableHeader>
          <TableBody>
            {tableBody(props)}
          </TableBody>
        </DataTable>
      </CardText>
    </Card>);
};

const messagePropType = PropTypes.shape({
  adId: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  reward: PropTypes.number.isRequired,
  expiresIn: PropTypes.number.isRequired,
  probability: PropTypes.string.isRequired,
  success: PropTypes.bool
}).isRequired;

MessagesTableCard.propTypes = {
  messages: PropTypes.arrayOf(messagePropType).isRequired,
  gameId: PropTypes.string.isRequired,
  onGetMessages: PropTypes.func.isRequired,
  onSolveMessage: PropTypes.func.isRequired,
};

export default MessagesTableCard;
