import React from 'react';
import {Card, CardText, CardTitle} from 'react-md/lib/Cards/index';
import PropTypes from 'prop-types';

export const GameInfoCard = props => {
  return <Card className="md-cell md-cell--4 md-cell--6-tablet">
    <CardTitle title="Game info" />
    <CardText className="md-grid">
      <div className="md-cell md-cell--12">
        <span className="md-cell md-cell--6"><b>Lives:</b> {props.lives} </span>
        <span className="md-cell md-cell--6"><b>Gold:</b> {props.gold} </span>
      </div>
      <div className="md-cell md-cell--12">
        <span className="md-cell md-cell--6"><b>Level:</b> {props.level} </span>
        <span className="md-cell md-cell--6"><b>Score:</b> {props.score} </span>
      </div>
      <div className="md-cell md-cell--12">
        <span className="md-cell md-cell--6"><b>High score:</b> {props.highScore} </span>
        <span className="md-cell md-cell--6"><b>Turn:</b> {props.turn} </span>
      </div>
    </CardText>
  </Card>;
};

GameInfoCard.defaultProps = {
  lives: 0,
  gold: 0,
  level: 0,
  score: 0,
  highScore: 0,
  turn: 0,
};

GameInfoCard.propTypes = {
  lives: PropTypes.number.isRequired,
  gold: PropTypes.number.isRequired,
  level: PropTypes.number.isRequired,
  score: PropTypes.number.isRequired,
  highScore: PropTypes.number.isRequired,
  turn: PropTypes.number.isRequired,
};
