import {connect} from 'react-redux';
import {Game} from '../components/Game';
import {
  GAME_REQUESTED,
  ITEM_PURCHASE_REQUESTED,
  ITEMS_REQUESTED,
  MESSAGE_SOLVE_REQUESTED,
  MESSAGES_REQUESTED,
  REPUTATION_REQUESTED
} from '../actions/GameActions';

const mapStateToProps = (state) => ({
  game: state.game
});

const mapDispatchToProps = (dispatch) => ({
  onCreateGame: () => dispatch({type: GAME_REQUESTED}),
  onGetReputation: (gameId) => dispatch({type: REPUTATION_REQUESTED, payload: {gameId}}),
  onGetMessages: (gameId) => dispatch({type: MESSAGES_REQUESTED, payload: {gameId}}),
  onSolveMessage: (gameId, adId) => dispatch({type: MESSAGE_SOLVE_REQUESTED, payload: {gameId, adId}}),
  onGetItems: (gameId) => dispatch({type: ITEMS_REQUESTED, payload: {gameId}}),
  onPurchaseItem: (gameId, itemId) => dispatch({type: ITEM_PURCHASE_REQUESTED, payload: {gameId, itemId}}),
});

export default connect(mapStateToProps, mapDispatchToProps)(Game);
