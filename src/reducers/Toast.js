import {ERROR_RECEIVED, TOAST_DISMISSED} from '../actions/GameActions';

const initialState = [];

const toasts = (state = initialState, action) => {
  switch (action.type) {
    case ERROR_RECEIVED: {
      let text;
      if (action.payload.error) {
        text = action.payload.error;
      } else if (action.payload.status) {
        text = action.payload.status;
      }
      return [{text}];
    }
    case TOAST_DISMISSED: {
      const [, ...toasts] = state;
      return toasts;
    }
    default:
      return state;
  }
};

export default toasts;
