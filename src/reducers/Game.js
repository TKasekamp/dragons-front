import {
  GAME_REQUESTED,
  GAME_RETRIEVED,
  MESSAGE_SOLVE_SUCCEEDED,
  MESSAGES_RETRIVED,
  REPUTATION_RETRIEVED, ITEMS_RETRIEVED, ITEM_PURCHASE_SUCCEEDED
} from '../actions/GameActions';

const initialState = {};

export const dummyStates = {
  reputation: {},
  messages: [],
  items: []
};

const logs = (state = initialState, action) => {
    switch (action.type) {
      case GAME_REQUESTED: {
        return initialState;
      }
      case GAME_RETRIEVED: {
        return {...action.payload, ...dummyStates};
      }
      case REPUTATION_RETRIEVED: {
        return {...state, reputation: action.payload};
      }
      case MESSAGES_RETRIVED: {
        return {...state, messages: action.payload};
      }
      case MESSAGE_SOLVE_SUCCEEDED: {
        // Manual remap as the API sends unrelated stuff in same payload
        const gameInfo = {
          turn: action.payload.response.turn,
          gold: action.payload.response.gold,
          lives: action.payload.response.lives,
          score: action.payload.response.score,
          highScore: action.payload.response.highScore
        };

        const messages = state.messages.map(m => {
          if (m.adId === action.payload.adId) {
            m.success = action.payload.response.success;
            m.message = action.payload.response.message;
          }
          return m;
        });

        return {...state, ...gameInfo, messages};
      }
      case ITEMS_RETRIEVED: {
        return {...state, items: action.payload};
      }
      case ITEM_PURCHASE_SUCCEEDED: {
        // Manual remap as the API sends unrelated stuff in same payload
        const gameInfo = {
          turn: action.payload.response.turn,
          gold: action.payload.response.gold,
          lives: action.payload.response.lives,
          level: action.payload.response.level,
        };

        const items = state.items.map(item => {
          if (item.id === action.payload.itemId) {
            item.shoppingSuccess = action.payload.response.shoppingSuccess;
          }
          return item;
        });

        return {...state, ...gameInfo, items};
      }
      default:
        return state;
    }
  }
;

export default logs;
