import {combineReducers} from 'redux';
import game from './Game';
import toasts from './Toast';

export default combineReducers({
  game,
  toasts
});
