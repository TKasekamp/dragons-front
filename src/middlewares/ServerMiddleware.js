import {createGame, getItems, getMessages, getReputation, purchaseItem, solveMessage} from '../api/api';
import {
  GAME_REQUESTED,
  ITEM_PURCHASE_REQUESTED,
  ITEMS_REQUESTED,
  MESSAGE_SOLVE_REQUESTED,
  MESSAGES_REQUESTED,
  REPUTATION_REQUESTED
} from '../actions/GameActions';

const ACTION_TYPE_TO_SERVER_ACTION = {
  [GAME_REQUESTED]: createGame,
  [REPUTATION_REQUESTED]: getReputation,
  [ITEMS_REQUESTED]: getItems,
  [ITEM_PURCHASE_REQUESTED]: purchaseItem,
  [MESSAGES_REQUESTED]: getMessages,
  [MESSAGE_SOLVE_REQUESTED]: solveMessage
};

const serverMiddleware = (store) => (next) => (action) => {
  const serverAction = ACTION_TYPE_TO_SERVER_ACTION[action.type];
  if (serverAction) {
    serverAction(action.payload)(store.dispatch);
  }
  return next(action);
};

export default serverMiddleware;
