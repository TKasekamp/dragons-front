import {TOAST_DISMISSED} from '../../actions/GameActions';
import toast from '../../reducers/Toast';

const action = {
  type: 'ERROR_RECEIVED',
  payload: {
    error: 'No ad by this ID exists'
  }
};

const actionStatus = {
  type: 'ERROR_RECEIVED',
  payload: {
    status: 'Game over'
  }
};

it('sets error as state', () => {
  const stateWithRequest = toast(undefined, action);
  expect(stateWithRequest).toEqual([{text: action.payload.error}]);
});


it('sets status as state', () => {
  const stateWithRequest = toast(undefined, actionStatus);
  expect(stateWithRequest).toEqual([{text: actionStatus.payload.status}]);
});


it('dismisses first toast', () => {
  const stateWithRequest = toast([{text: '1'}, {text: '2'}], {type: TOAST_DISMISSED});
  expect(stateWithRequest).toEqual([{text: '2'}]);
});
