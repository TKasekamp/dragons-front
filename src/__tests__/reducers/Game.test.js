import game, {dummyStates} from '../../reducers/Game';
import {
  GAME_REQUESTED,
  GAME_RETRIEVED,
  REPUTATION_RETRIEVED,
  MESSAGES_RETRIVED,
  MESSAGE_SOLVE_SUCCEEDED, ITEMS_RETRIEVED, ITEM_PURCHASE_SUCCEEDED
} from '../../actions/GameActions';

const initState = {};
const gamePayload = {'gameId': 'KqfXpSiq', 'lives': 3, 'gold': 0, 'level': 0, 'score': 0, 'highScore': 3801, 'turn': 0};


const messages = [{
  'adId': 'esVIPTBW',
  'message': 'Help Veniaminu Michaelson to sell an unordinary pan on the local market',
  'reward': 16,
  'expiresIn': 6,
  'encrypted': null,
  'probability': 'Piece of cake'
}, {
  'adId': 'g589L2VB',
  'message': 'Help Chiamaka Sandford to sell an unordinary wagon on the local market',
  'reward': 30,
  'expiresIn': 6,
  'encrypted': null,
  'probability': 'Piece of cake'
}];

const solveResponse = {
  'success': true,
  'lives': 1,
  'gold': 32,
  'score': 32,
  'highScore': 3801,
  'turn': 1,
  'message': 'You successfully solved the mission!'
};

const items = [
  {'id': 'hpot', 'name': 'Healing potion', 'cost': 50},
  {'id': 'cs', 'name': 'Claw Sharpening', 'cost': 100}
];

const itemResponse = {'shoppingSuccess': false, 'gold': 234, 'lives': 6, 'level': 5, 'turn': 11};


let retrievedGame;
describe('GameReducer', () => {
  beforeEach(() => {
    retrievedGame = game(initState, {type: GAME_RETRIEVED, payload: gamePayload});
  });

  it('has nothing initially', () => {
    expect(game(undefined, {})).toEqual(initState);
  });

  it('clears existing state when requested', () => {
    const stateWithRequest = game({id: 'asdasd'}, {type: GAME_REQUESTED});
    expect(stateWithRequest).toEqual({});
  });

  it('adds game to state', () => {
    expect(retrievedGame).toEqual({...gamePayload, ...dummyStates});
  });

  describe('REPUTATION_RETRIVED', () => {
    it('replaces reputation', () => {
      const reputation = {'people': 0, 'state': 0, 'underworld': 0};
      const stateWithRequest = game(retrievedGame, {type: REPUTATION_RETRIEVED, payload: reputation});
      expect(stateWithRequest).toMatchObject({reputation});
    });
  });

  describe('MESSAGES_RETRIVED', () => {
    it('replaces messages', () => {
      const stateWithRequest = game(retrievedGame, {type: MESSAGES_RETRIVED, payload: messages});
      expect(stateWithRequest).toMatchObject({messages});
    });
  });

  describe(`${MESSAGE_SOLVE_SUCCEEDED}`, () => {
    const stateWithMessages = game(retrievedGame, {type: MESSAGES_RETRIVED, payload: messages});
    const stateWithRequest = game(stateWithMessages, {
      type: MESSAGE_SOLVE_SUCCEEDED,
      payload: {adId: 'esVIPTBW', response: solveResponse}
    });

    it('updates the message state with success and text', () => {
      expect(stateWithRequest.messages[0]).toMatchObject({
        adId: 'esVIPTBW',
        success: solveResponse.success,
        message: solveResponse.message
      });
    });

    it('updates the game body', () => {
      expect(stateWithRequest).toMatchObject({
        lives: solveResponse.lives,
        gold: solveResponse.gold,
        score: solveResponse.score,
        highScore: solveResponse.highScore,
        turn: solveResponse.turn
      });
    });
  });

  describe(`${ITEMS_RETRIEVED}`, () => {
    it('replaces items', () => {
      const stateWithRequest = game(retrievedGame, {type: ITEMS_RETRIEVED, payload: items});
      expect(stateWithRequest).toMatchObject({items});
    });
  });

  describe(`${ITEM_PURCHASE_SUCCEEDED}`, () => {
    const stateWithItems = game(retrievedGame, {type: ITEMS_RETRIEVED, payload: items});
    const stateWithRequest = game(stateWithItems, {
      type: ITEM_PURCHASE_SUCCEEDED,
      payload: {itemId: 'hpot', response: itemResponse}
    });

    it('updates the item state with shopping success', () => {
      expect(stateWithRequest.items[0]).toMatchObject({
        id: 'hpot',
        shoppingSuccess: itemResponse.shoppingSuccess,
      });
    });

    it('updates the game body', () => {
      expect(stateWithRequest).toMatchObject({
        lives: itemResponse.lives,
        gold: itemResponse.gold,
        level: itemResponse.level,
        turn: itemResponse.turn
      });
    });
  });
});
