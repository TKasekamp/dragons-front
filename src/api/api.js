import {SERVER_URL} from '../constants';
import jsonAjax from './JSONAjaxRequest';
import {
  ERROR_RECEIVED,
  GAME_RETRIEVED,
  ITEM_PURCHASE_SUCCEEDED,
  ITEMS_RETRIEVED,
  MESSAGE_SOLVE_SUCCEEDED,
  MESSAGES_RETRIVED,
  REPUTATION_RETRIEVED
} from '../actions/GameActions';

/* eslint-disable no-console */
export const createGame = () => (dispatch) => {
  jsonAjax(
    SERVER_URL + '/game/start',
    'POST',
    null,
    (game) => dispatch({type: GAME_RETRIEVED, payload: game}),
    (error) => dispatch({type: ERROR_RECEIVED, payload: error})
  );
};

export const getReputation = ({gameId}) => (dispatch) => {
  jsonAjax(
    SERVER_URL + `/${gameId}/investigate/reputation`,
    'POST',
    null,
    (reputation) => dispatch({type: REPUTATION_RETRIEVED, payload: reputation}),
    (error) => dispatch({type: ERROR_RECEIVED, payload: error})
  );
};

export const getMessages = ({gameId}) => (dispatch) => {
  jsonAjax(
    SERVER_URL + `/${gameId}/messages`,
    'GET',
    null,
    (messages) => dispatch({type: MESSAGES_RETRIVED, payload: messages}),
    (error) => dispatch({type: ERROR_RECEIVED, payload: error})
  );
};

export const solveMessage = ({gameId, adId}) => (dispatch) => {
  jsonAjax(
    SERVER_URL + `/${gameId}/solve/${adId}`,
    'POST',
    null,
    (response) => dispatch({type: MESSAGE_SOLVE_SUCCEEDED, payload: {adId, response}}),
    (error) => dispatch({type: ERROR_RECEIVED, payload: error})
  );
};

export const getItems = ({gameId}) => (dispatch) => {
  jsonAjax(
    SERVER_URL + `/${gameId}/shop`,
    'GET',
    null,
    (items) => dispatch({type: ITEMS_RETRIEVED, payload: items}),
    (error) => dispatch({type: ERROR_RECEIVED, payload: error})
  );
};

export const purchaseItem = ({gameId, itemId}) => (dispatch) => {
  jsonAjax(
    SERVER_URL + `/${gameId}/shop/buy/${itemId}`,
    'POST',
    null,
    (response) => dispatch({type: ITEM_PURCHASE_SUCCEEDED, payload: {itemId, response}}),
    (error) => dispatch({type: ERROR_RECEIVED, payload: error})
  );
};
